from django.db import models

class Device(models.Model):
	deviceName = models.TextField()
	deviceLocation = models.TextField()
	deviceDescription = models.TextField()

class Notification(models.Model):
	sourceDeviceName = models.TextField()
	
	sourceDeviceId = models.ForeignKey(
	Device,
	on_delete=models.CASCADE,
	null=True,
	verbose_name="the id of the device reporting.")
	
	degreesC = models.DecimalField(max_digits=6, decimal_places=3, null=True)
	degreesF = models.DecimalField(max_digits=6, decimal_places=3, null=True)
	status = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
