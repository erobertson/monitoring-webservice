from django.shortcuts import render
from django.http import HttpResponse

from .models import Notification
import re

# Create your views here.
def index(request):
    # return HttpResponse('Hello from Python!')
    return render(request, 'index.html')

def filterInput(input):
    #allow alphanumeric, space, - and period
    return re.sub(r'[^a-zA-Z0-9 -.]+', '', input)

def logService(request):
    deviceName = ''
    devStatus = ''
    reportC = ''
    reportF = ''
    if(request.method == 'GET' and 'deviceName' in request.GET
       and 'status' in request.GET):
        deviceName = filterInput(request.GET["deviceName"])
        devStatus = filterInput(request.GET["status"])
	postNotification = Notification(sourceDeviceName=deviceName,
                             status=devStatus,
                             created="now")
	#sourceDeviceId = Notification.Device.objects.get(name=deviceName),
	postNotification.Device = deviceName
        if('reportC' in request.GET and 'reportF' in request.GET): 
            reportC = filterInput(request.GET['reportC'])
            reportF = filterInput(request.GET['reportF'])
            postNotification.degreesC = reportC
            postNotification.degreesF = reportF


	postNotification.save()

    recentNotifications = Notification.objects.order_by('-created')[:100] #limit to 500
    return render(request, 'logService', {'recentNotifications':recentNotifications})


def temperatureReport(request):
    #refactor to look at deviceLocation instead of device name, which can change
    homeNotifications = Notification.objects.filter(degreesC__isnull=False).filter(degreesF__isnull=False).exclude(sourceDeviceName__icontains='work').exclude(sourceDeviceName__icontains='RaspberryPi').order_by('created')
    officeNotifications = Notification.objects.filter(degreesC__isnull=False).filter(degreesF__isnull=False).filter(sourceDeviceName__icontains='work').order_by('created')
    return render(request, 'temperatureReport', {'homeNotifications':homeNotifications,'officeNotifications':officeNotifications})
