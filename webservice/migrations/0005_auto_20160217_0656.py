# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-17 11:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webservice', '0004_remove_notification_sourcedevice'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notification',
            name='testField',
        ),
        migrations.AddField(
            model_name='notification',
            name='sourceDevice',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
