from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

import webservice.views

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    url(r'^$', webservice.views.index, name='index'),
    #url(r'^db', webservice.views.db, name='db'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logService/$', webservice.views.logService, name='logService'),
	url(r'^temperatureReport/$', webservice.views.temperatureReport, name='temperatureReport'),
]
